package wyrmstatus

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
)

type BookwyrmClient struct {
	BaseURL string
}

func NewClient(instanceUrl string) BookwyrmClient {
	return BookwyrmClient{
		BaseURL: instanceUrl,
	}
}

type ClientRequestError struct {
	url    string
	status int
}

func (err *ClientRequestError) Error() string {
	return fmt.Sprintf("Client request to %s returned status code %d", err.url, err.status)
}

func (bc BookwyrmClient) get(urlPath string) ([]byte, error) {
	url := fmt.Sprintf("%s/%s", bc.BaseURL, urlPath)
	res, err := http.Get(url)
	if res == nil {
		return []byte{}, err
	}

	if res.StatusCode != http.StatusOK {
		return []byte{}, &ClientRequestError{
			url:    url,
			status: res.StatusCode,
		}
	}
	defer res.Body.Close()

	return io.ReadAll(res.Body)
}

func (bc BookwyrmClient) GetUser(user string) (User, error) {
	data, err := bc.get(fmt.Sprintf("user/%s.json", user))
	if err != nil {
		return User{}, err
	}

	var jsonData struct {
		Username string `json:"preferredUsername"`
		Summary  string `json:"summary"`
		Icon     struct {
			URL string `json:"url"`
		} `json:"icon"`
	}

	err = json.Unmarshal(data, &jsonData)
	return User{
		Username: jsonData.Username,
		Summary:  jsonData.Summary,
		IconURL:  jsonData.Icon.URL,
	}, err
}

func (bc BookwyrmClient) GetUserFollowers(user string, page int) ([]string, error) {
	data, err := bc.get(fmt.Sprintf("user/%s/followers.json?page=%d", user, page))
	if err != nil {
		return []string{}, err
	}

	var jsonData struct {
		Followers []string `json:"orderedItems"`
	}

	err = json.Unmarshal(data, &jsonData)
	return jsonData.Followers, err
}

func (bc BookwyrmClient) GetUserFollowing(user string, page int) ([]string, error) {
	data, err := bc.get(fmt.Sprintf("user/%s/following.json?page=%d", user, page))
	if err != nil {
		return []string{}, err
	}

	var jsonData struct {
		Following []string `json:"orderedItems"`
	}

	err = json.Unmarshal(data, &jsonData)
	return jsonData.Following, err
}

func (bc BookwyrmClient) GetUserShelf(user, shelf string) (Shelf, error) {
	data, err := bc.get(fmt.Sprintf("user/%s/shelf/%s.json", user, shelf))
	if err != nil {
		return Shelf{}, err
	}

	var jsonData struct {
		Name        string `json:"name"`
		TotalItems  int    `json:"totalItems"`
		LastPageURL string `json:"last"`
	}

	err = json.Unmarshal(data, &jsonData)
	if err != nil {
		return Shelf{}, err
	}

	numCharIndex := strings.Index(jsonData.LastPageURL, "=")
	lastPageStr := jsonData.LastPageURL[numCharIndex+1:]
	lastPage, err := strconv.Atoi(lastPageStr)

	return Shelf{
		Name:       jsonData.Name,
		TotalItems: jsonData.TotalItems,
		TotalPages: lastPage,
	}, err
}

func (bc BookwyrmClient) GetUserShelfPage(user, shelf string, page int) ([]Book, error) {
	data, err := bc.get(fmt.Sprintf("user/%s/shelf/%s.json?page=%d", user, shelf, page))
	if err != nil {
		return []Book{}, err
	}

	var jsonData struct {
		Items []bookJson `json:"orderedItems"`
	}

	err = json.Unmarshal(data, &jsonData)
	if err != nil {
		return []Book{}, err
	}

	var books []Book
	for _, bookData := range jsonData.Items {
		books = append(books, Book{
			ID:            bookData.BookURL[strings.LastIndex(bookData.BookURL, "/")+1:],
			Title:         bookData.Title,
			Subtitle:      bookData.Subtitle,
			Description:   bookData.Description,
			AuthorsURL:    bookData.AuthorsURL,
			PublishedDate: bookData.PublishedDate,
			CoverURL:      bookData.Cover.URL,
			ISBN:          bookData.ISBN,
			Pages:         bookData.Pages,
			Publishers:    bookData.Publishers,
		})
	}

	return books, err
}

func (bc BookwyrmClient) GetBook(id string) (Book, error) {
	data, err := bc.get(fmt.Sprintf("book/%s.json", id))
	if err != nil {
		return Book{}, err
	}

	var jsonData bookJson

	err = json.Unmarshal(data, &jsonData)
	if err != nil {
		return Book{}, err
	}

	return Book{
		ID:            jsonData.BookURL[strings.LastIndex(jsonData.BookURL, "/")+1:],
		Title:         jsonData.Title,
		Subtitle:      jsonData.Subtitle,
		Description:   jsonData.Title,
		AuthorsURL:    jsonData.AuthorsURL,
		PublishedDate: jsonData.PublishedDate,
		CoverURL:      jsonData.Cover.URL,
		ISBN:          jsonData.ISBN,
		Pages:         jsonData.Pages,
		Publishers:    jsonData.Publishers,
	}, err
}

// for some reason the bookwyrm JSON API only allows to search books,
// even when specifying type=list/user as a parameter.
// see <https://github.com/bookwyrm-social/bookwyrm/blob/597378bb78c868a5c68667e07155b5df00b31c1d/bookwyrm/views/search.py#L29>
// might be interesting to fix it and send a PR to upstream
// func (bc BookwyrmClient) SearchBook(query string) (string, error) {
// 	bc.get(fmt.Sprintf("search.json?q=%s", strings.ReplaceAll(query, " ", "+")))
// }
