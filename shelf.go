package wyrmstatus

type Shelf struct {
	Name       string
	TotalItems int
	TotalPages int
}
