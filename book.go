package wyrmstatus

type Book struct {
	ID            string
	Title         string
	Subtitle      string
	Description   string
	AuthorsURL    []string
	PublishedDate string
	CoverURL      string
	ISBN          string
	Pages         int
	Publishers    []string
}

type bookJson struct {
	BookURL       string   `json:"id"`
	Title         string   `json:"title"`
	Subtitle      string   `json:"subtitle"`
	Description   string   `json:"description"`
	AuthorsURL    []string `json:"authors"`
	PublishedDate string   `json:"publishedDate"`
	Cover         struct {
		URL string `json:"url"`
	} `json:"cover"`
	ISBN       string   `json:"isbn13"`
	Pages      int      `json:"pages"`
	Publishers []string `json:"publishers"`
}
