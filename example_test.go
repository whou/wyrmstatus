package wyrmstatus_test

import "codeberg.org/whou/wyrmstatus"

func Example() {
	// Create a Bookwyrm client with an instance URL
	client := wyrmstatus.NewClient("https://bookwyrm.social")

	// With the client, you can fetch multiple kinds of information from
	// books and users registered in that instance

	// Fetching info about a user profile
	user, err := client.GetUser("mouse")
	if err != nil {
		panic(err)
	}

	println("username:", user.Username)
	println("icon:", user.IconURL)
	println("summary:", user.Summary)
	print("\n")

	// It's also possible to gather information about a certain shelf
	shelf, err := client.GetUserShelf("mouse", "to-read")
	if err != nil {
		panic(err)
	}

	println("shelf:", shelf.Name)
	println("pages:", shelf.TotalPages)
	println("items:", shelf.TotalItems)
	print("\n")

	// To get the contents of a user book shelf/list we need to specify which
	// page of items in that shelf we want to get
	books, err := client.GetUserShelfPage("mouse", "to-read", 1)
	if err != nil {
		panic(err)
	}

	// Now we have an array which each book of the first page of the
	// "to-read" shelf
	for _, book := range books {
		println(book.Title)

		if book.Subtitle != "" {
			println(book.Subtitle)
		}

		if book.Description != "" {
			println(book.Description)
		}

		print("author(s): [")
		for i, author := range book.AuthorsURL {
			print("\"", author, "\"")
			if i < len(book.AuthorsURL) - 1 {
				print(", ")
			}
		}
		println("]")

		println("ID:", book.ID)
		println("ISBN:", book.ISBN)
		println("publised on", book.PublishedDate)
		println("pages:", book.Pages)
		println("cover:", book.CoverURL)

		print("publisher(s): ")
		for i, author := range book.Publishers {
			print(author)
			if i < len(book.Publishers) - 1 {
				print(", ")
			}
		}
		print("\n\n")
	}

	// It's also possible to get information about a specific book if we have
	// the book ID
	book, err := client.GetBook(books[0].ID)
	if err != nil {
		panic(err)
	}

	println(book.Title)
	println("ISBN:", book.ISBN)
}

