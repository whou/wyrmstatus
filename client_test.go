package wyrmstatus_test

import (
	"testing"

	"codeberg.org/whou/wyrmstatus"
)

const (
	TEST_INSTANCE = "https://bookwyrm.social"
	TEST_USER     = "mouse"
)

var client wyrmstatus.BookwyrmClient = wyrmstatus.NewClient(TEST_INSTANCE)

func TestGetUser(t *testing.T) {
	user, err := client.GetUser(TEST_USER)
	if err != nil {
		t.Fatal(err)
	}

	if user.Username != TEST_USER {
		t.Errorf("username \"%s\" does not match test user \"%s\"", user.Username, TEST_USER)
	}
}

func TestGetUserFollowers(t *testing.T) {
	_, err := client.GetUserFollowers(TEST_USER, 1)
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetUserFollowing(t *testing.T) {
	_, err := client.GetUserFollowing(TEST_USER, 1)
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetUserShelf(t *testing.T) {
	_, err := client.GetUserShelf(TEST_USER, "to-read")
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetUserShelfPage(t *testing.T) {
	_, err := client.GetUserShelfPage(TEST_USER, "to-read", 1)
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetBook(t *testing.T) {
	const testId = "1088004"
	const testTitle = "Androids"
	const testSubtitle = "The Team That Built the Android Operating System"

	book, err := client.GetBook(testId)
	if err != nil {
		t.Fatal(err)
	}

	if book.ID != testId {
		t.Errorf("book ID \"%s\" does not match test ID \"%s\"", book.ID, testId)
	}
	if book.Title != testTitle {
		t.Errorf("book title \"%s\" does not match test title \"%s\"", book.Title, testTitle)
	}
	if book.Subtitle != testSubtitle {
		t.Errorf("book subtitle \"%s\" does not match test subtitle \"%s\"", book.Subtitle, testSubtitle)
	}
}
