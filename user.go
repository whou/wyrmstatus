package wyrmstatus

type User struct {
	Username string
	Summary  string
	IconURL  string
}
